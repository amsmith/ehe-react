import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

import { Provider } from 'react-redux';
import { StripeProvider } from 'react-stripe-elements';

import { ToastContainer } from 'react-toastify';
import Header from './components/shared/Header';
import InnerShowcase from './components/shared/InnerShowcase';
import ServiceListing from './components/service/service-listing/ServiceListing';
import ServiceSearchListing from './components/service/service-listing/ServiceSearchListing';
import ServiceDetail from './components/service/service-detail/ServiceDetail';
import ServiceUpdate from './components/service/service-detail/ServiceUpdate';
import ServiceCreate from './components/service/service-create/ServiceCreate';
import ServiceManage from './components/service/service-manage/ServiceManage';
import BookingManage from './components/booking/booking-manage/BookingManage';
import Landing from './components/Landing';

import Login from './components/login/Login';
import Register from './components/register/Register';

import { ProtectedRoute } from './components/shared/auth/ProtectedRoute';
import { LoggedInRoute } from './components/shared/auth/LoggedInRoute';

import * as actions from './actions';

import './App.css';
import Footer from './components/shared/Footer';

const store = require('./reducers').init();

class App extends Component {
  componentWillMount() {
    this.checkAuthState();
  }

  checkAuthState() {
    store.dispatch(actions.checkAuthState());
  }

  logout() {
    store.dispatch(actions.logout());
  }

  render() {
    return (
      <StripeProvider apiKey='pk_test_8wHmm3PNmW8VNhrXQVkasyGm'>
        <Provider store={store}>
          <BrowserRouter>
            <div className='App'>
              <ToastContainer />
              <Header logout={this.logout} />
              <div>
                <Switch>
                  <Route
                    exact
                    path='/'
                    component={Landing}
                    // render={() => <Redirect to='/services' />}
                  />
                  <Route exact path='/services' component={ServiceListing} />
                  <Route exact path='/login' component={Login} />
                  <LoggedInRoute exact path='/register' component={Register} />
                  <ProtectedRoute
                    exact
                    path='/services/manage'
                    component={ServiceManage}
                  />
                  <ProtectedRoute
                    exact
                    path='/services/new'
                    component={ServiceCreate}
                  />
                  <ProtectedRoute
                    exact
                    path='/bookings/manage'
                    component={BookingManage}
                  />
                  <Route exact path='/services/:id' component={ServiceDetail} />
                  <Route
                    exact
                    path='/services/:id/edit'
                    component={ServiceUpdate}
                  />
                  <Route
                    exact
                    path='/services/:city/experts'
                    component={ServiceSearchListing}
                  />
                </Switch>
              </div>
              <Footer />
            </div>
          </BrowserRouter>
        </Provider>
      </StripeProvider>
    );
  }
}

export default App;
