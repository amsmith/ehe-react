import axios from 'axios';
import authService from '../services/auth-service';
import axiosService from '../services/axios-service';

import {
  FETCH_SERVICES_SUCCESS,
  FETCH_SERVICE_BY_ID_SUCCESS,
  FETCH_SERVICE_BY_ID_INIT,
  FETCH_SERVICES_INIT,
  FETCH_SERVICES_FAIL,
  FETCH_USER_BOOKINGS_INIT,
  FETCH_USER_BOOKINGS_SUCCESS,
  FETCH_USER_BOOKINGS_FAIL,
  UPDATE_SERVICE_SUCCESS,
  UPDATE_SERVICE_FAIL,
  RESET_SERVICE_ERRORS,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT,
  RELOAD_MAP,
  RELOAD_MAP_FINISH,
  UPDATE_BOOKINGS
} from './types';

const axiosInstance = axiosService.getInstance();

export const verifyServiceOwner = serviceId => {
  return axiosInstance.get(`/services/${serviceId}/verify-user`);
};

export const reloadMap = () => {
  return {
    type: RELOAD_MAP
  };
};

export const reloadMapFinish = () => {
  return {
    type: RELOAD_MAP_FINISH
  };
};

// SERVICE ACTIONS

const fetchServiceByIdInit = service => {
  return {
    type: FETCH_SERVICE_BY_ID_INIT
  };
};

const fetchServiceByIdSuccess = service => {
  return {
    type: FETCH_SERVICE_BY_ID_SUCCESS,
    service
  };
};

const fetchServicesSuccess = services => {
  return {
    type: FETCH_SERVICES_SUCCESS,
    services
  };
};

const fetchServicesInit = () => {
  return {
    type: FETCH_SERVICES_INIT
  };
};

const fetchServicesFail = errors => {
  return {
    type: FETCH_SERVICES_FAIL,
    errors
  };
};

export const fetchServices = city => {
  const url = city ? `services?city=${city}` : '/services';
  return dispatch => {
    dispatch(fetchServicesInit());

    axiosInstance
      .get(url)
      .then(res => res.data)
      .then(services => dispatch(fetchServicesSuccess(services)))
      .catch(({ response }) => {
        dispatch(fetchServicesFail(response.data.errors));
      });
  };
};

export const fetchServiceById = serviceId => {
  return dispatch => {
    dispatch(fetchServiceByIdInit());

    return axios
      .get(`/api/v1/services/${serviceId}`)
      .then(res => res.data)
      .then(service => {
        dispatch(fetchServiceByIdSuccess(service));
        return service;
      });
  };
};

export const resetServiceErrors = () => {
  return {
    type: RESET_SERVICE_ERRORS
  };
};

const updateServiceSuccess = updatedService => {
  return {
    type: UPDATE_SERVICE_SUCCESS,
    service: updatedService
  };
};

const updateServiceFail = errors => {
  return {
    type: UPDATE_SERVICE_FAIL,
    errors
  };
};

export const updateService = (id, serviceData) => dispatch => {
  return axiosInstance
    .patch(`/services/${id}`, serviceData)
    .then(res => res.data)
    .then(updatedService => {
      dispatch(updateServiceSuccess(updatedService));

      if (serviceData.city) {
        dispatch(reloadMap());
      }
    })
    .catch(({ response }) => dispatch(updateServiceFail(response.data.errors)));
};

export const createService = serviceData => {
  return axiosInstance
    .post('/services', serviceData)
    .then(res => res.data, err => Promise.reject(err.response.data.errors));
};

// BOOKING ACTIONS
const fetchUserBookingsInit = () => {
  return {
    type: FETCH_USER_BOOKINGS_INIT
  };
};

const fetchUserBookingsSuccess = userBookings => {
  return {
    type: FETCH_USER_BOOKINGS_SUCCESS,
    userBookings
  };
};

const fetchUserBookingsFail = errors => {
  return {
    type: FETCH_USER_BOOKINGS_FAIL,
    errors
  };
};

export const updateBookings = bookings => {
  return {
    type: UPDATE_BOOKINGS,
    bookings
  };
};

export const fetchUserBookings = () => {
  return dispatch => {
    dispatch(fetchUserBookingsInit());

    axiosInstance
      .get('/bookings/manage')
      .then(res => res.data)
      .then(userBookings => dispatch(fetchUserBookingsSuccess(userBookings)))
      .catch(({ response }) => {
        dispatch(fetchUserBookingsFail(response.data.errors));
      });
  };
};

// USER SERVICES ACTIONS
export const getUserServices = () => {
  return axiosInstance
    .get('/services/manage')
    .then(res => res.data, err => Promise.reject(err.response.data.errors));
};

export const deleteService = serviceId => {
  return axiosInstance
    .delete(`/services/${serviceId}`)
    .then(res => res.data, err => Promise.reject(err.response.data.errors));
};

// AUTH ACTIONS

const loginSuccess = () => {
  const username = authService.getUsername();
  return {
    type: LOGIN_SUCCESS,
    username
  };
};

const loginFailure = errors => {
  return {
    type: LOGIN_FAILURE,
    errors
  };
};

export const register = userData => {
  return axios
    .post('/api/v1/users/register', userData)
    .then(res => res.data, err => Promise.reject(err.response.data.errors));
};

export const checkAuthState = () => {
  return dispatch => {
    if (authService.isAuthenticated()) {
      dispatch(loginSuccess());
    }
  };
};

export const login = userData => {
  return dispatch => {
    return axios
      .post('/api/v1/users/auth', userData)
      .then(res => res.data)
      .then(token => {
        authService.saveToken(token);
        dispatch(loginSuccess());
      })
      .catch(({ response }) => {
        dispatch(loginFailure(response.data.errors));
      });
  };
};

export const logout = () => {
  authService.invalidateUser();
  return {
    type: LOGOUT
  };
};

export const createBooking = booking => {
  return axiosInstance
    .post('/bookings', booking)
    .then(res => res.data)
    .catch(({ response }) => Promise.reject(response.data.errors));
};

export const uploadImage = image => {
  const formData = new FormData();
  formData.append('image', image);

  return axiosInstance
    .post('/image-upload', formData)
    .then(json => {
      return json.data.imageUrl;
    })
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};

export const getPendingPayments = () => {
  return axiosInstance
    .get('/payments')
    .then(res => res.data)
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};

export const acceptPayment = payment => {
  return axiosInstance
    .post('/payments/accept', payment)
    .then(res => res.data)
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};

export const declinePayment = payment => {
  return axiosInstance
    .post('/payments/decline', payment)
    .then(res => res.data)
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};

// REVIEWS ACTIONS
export const createReview = (reviewData, bookingId) => {
  return axiosInstance
    .post(`/reviews?bookingId=${bookingId}`, reviewData)
    .then(res => res.data)
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};

export const getReviews = serviceId => {
  return axiosInstance
    .get(`/reviews?serviceId=${serviceId}`)
    .then(res => res.data)
    .catch(({ response }) => {
      Promise.reject(response.data.errors[0]);
    });
};
