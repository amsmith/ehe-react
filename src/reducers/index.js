import { serviceReducer, selectedServiceReducer } from './service-reducer';
import { userBookingsReducer } from './booking-reducer';

import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';

import { reducer as formReducer } from 'redux-form';
import { authReducer } from './auth-reducer';
import { rentalMapReducer } from './map-reducer';

export const init = () => {
  const reducer = combineReducers({
    services: serviceReducer,
    service: selectedServiceReducer,
    userBookings: userBookingsReducer,
    form: formReducer,
    auth: authReducer,
    map: rentalMapReducer
  });

  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

  return store;
};
