import {
  FETCH_SERVICES_SUCCESS,
  FETCH_SERVICE_BY_ID_SUCCESS,
  FETCH_SERVICE_BY_ID_INIT,
  FETCH_SERVICES_INIT,
  FETCH_SERVICES_FAIL,
  UPDATE_SERVICE_SUCCESS,
  UPDATE_SERVICE_FAIL,
  RESET_SERVICE_ERRORS
} from '../actions/types';

const INITIAL_STATE = {
  services: {
    data: [],
    errors: []
  },
  service: {
    data: {},
    errors: []
  }
};

export const serviceReducer = (state = INITIAL_STATE.services, action) => {
  switch (action.type) {
    case FETCH_SERVICES_SUCCESS:
      return { ...state, data: action.services };
    case FETCH_SERVICES_INIT:
      return { ...state, data: [], errors: [] };
    case FETCH_SERVICES_FAIL:
      return { ...state, data: [], errors: action.errors };
    default:
      return state;
  }
};

export const selectedServiceReducer = (
  state = INITIAL_STATE.service,
  action
) => {
  switch (action.type) {
    case FETCH_SERVICE_BY_ID_INIT:
      return { ...state, data: {} };
    case FETCH_SERVICE_BY_ID_SUCCESS:
      return { ...state, data: action.service };
    case UPDATE_SERVICE_SUCCESS:
      return { ...state, data: action.service };
    case UPDATE_SERVICE_FAIL:
      return { ...state, errors: action.errors };
    case RESET_SERVICE_ERRORS:
      return { ...state, errors: [] };
    default:
      return state;
  }
};
