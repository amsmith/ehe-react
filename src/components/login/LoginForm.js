import React from "react";
import { Field, reduxForm } from "redux-form";
import Eheinput from "../shared/form/Eheinput";
import EheResErrors from "../shared/form/EheResErrors";
import { required, minLength4 } from "../shared/form/validators";

const LoginForm = props => {
  const { handleSubmit, pristine, submitting, submitCb, valid, errors } = props;

  return (
    <form onSubmit={handleSubmit(submitCb)}>
      <Field
        name="email"
        type="email"
        label="Email"
        className="form-control"
        component={Eheinput}
        validate={[required, minLength4]}
      />
      <Field
        name="password"
        type="password"
        label="Password"
        className="form-control"
        component={Eheinput}
        validate={[required, minLength4]}
      />
      <button
        className="btn btn-ehe btn-form"
        type="submit"
        disabled={!valid || pristine || submitting}
      >
        Log In
      </button>
      <EheResErrors errors={errors} />
    </form>
  );
};

export default reduxForm({
  form: "loginForm"
})(LoginForm);
