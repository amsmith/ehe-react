import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import StarRatings from 'react-star-ratings';
import * as actions from '../../actions';

export default class ReviewModal extends Component {
  state = {
    open: false,
    text: '',
    rating: 3
  };

  closeModal = () => {
    this.setState({ open: false });
  };

  publishReview = () => {
    const { text, rating } = this.state;
    const { bookingId, onReviewCreated } = this.props;

    actions.createReview({ text, rating }, bookingId).then(review => {
      onReviewCreated(review);
    });
    this.closeModal();
  };

  openModal = () => {
    this.setState({ open: true });
  };

  handleTextChange = event => {
    this.setState({ text: event.target.value });
  };

  changeRating = (newRating, name) => {
    this.setState({
      rating: newRating
    });
  };

  render() {
    const { open, text, rating } = this.state;

    return (
      <React.Fragment>
        <button className='ml-2 btn btn-ehe' onClick={this.openModal}>
          Review
        </button>
        <Modal
          open={open}
          onClose={this.closeModal}
          little
          classNames={{ modal: 'review-modal' }}
        >
          <h4 className='modal-title title'>Write Review </h4>
          <div className='modal-body'>
            <textarea
              value={text}
              onChange={this.handleTextChange}
              className='form-control mb-2'
              placeholder='Write a review of the work performed for this job.'
              rows={3}
              cols={50}
            />
            <StarRatings
              rating={rating}
              starRatedColor='#ff9f3a'
              starHoverColor='#ff9f3a'
              starDimension='25px'
              starSpacing='1px'
              changeRating={this.changeRating}
              numberOfStars={5}
              name='rating'
            />
          </div>
          <div className='modal-footer'>
            <button
              disabled={!text || !rating}
              onClick={this.publishReview}
              type='button'
              className='btn btn-ehe'
            >
              Publish
            </button>
            <button
              type='button'
              onClick={this.closeModal}
              className='btn btn-ehe'
            >
              Cancel
            </button>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}
