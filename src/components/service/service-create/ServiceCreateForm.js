import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Eheinput from '../../shared/form/Eheinput';
import EheTextArea from '../../shared/form/EheTextArea';
import EheSelect from '../../shared/form/EheSelect';
import EheFileUpload from '../../shared/form/EheFileUpload';
import EheResErrors from '../../shared/form/EheResErrors';
// import { required, minLength4 } from "../shared/form/validators";

const ServiceCreateForm = props => {
  const {
    handleSubmit,
    pristine,
    submitting,
    submitCb,
    valid,
    options,
    errors
  } = props;
  return (
    <form onSubmit={handleSubmit(submitCb)}>
      <Field
        name='company'
        type='text'
        label='Company'
        className='form-control'
        component={Eheinput}
      />
      <Field
        name='description'
        type='text'
        label='Description'
        rows='5'
        className='form-control'
        component={EheTextArea}
      />
      <Field
        options={options}
        name='category'
        label='Category'
        className='form-control'
        component={EheSelect}
      />
      <Field
        name='image'
        label='Image Upload'
        className='form-control'
        component={EheFileUpload}
      />
      <Field
        name='city'
        type='text'
        label='City'
        className='form-control'
        component={Eheinput}
      />
      <Field
        name='state'
        type='text'
        label='State'
        className='form-control'
        component={Eheinput}
      />
      <Field
        name='price'
        type='text'
        label='Price'
        className='form-control'
        symbol='$'
        component={Eheinput}
      />
      <button
        className='btn btn-ehe btn-form'
        type='submit'
        disabled={!valid || pristine || submitting}
      >
        Create Service
      </button>
      <EheResErrors errors={errors} />
    </form>
  );
};

export default reduxForm({
  form: 'serviceCreateForm',
  initialValues: { category: 'Auto Detailing' }
})(ServiceCreateForm);
