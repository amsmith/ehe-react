import React, { Component } from 'react';
import ServiceCreateForm from './ServiceCreateForm';
import { Redirect } from 'react-router-dom';

import * as actions from '../../../actions';

export default class ServiceCreate extends Component {
  constructor() {
    super();

    this.state = {
      redirect: false,
      errors: []
    };

    this.serviceCategories = [
      'Auto Detailing',
      'Housekeeping',
      'Landscaping',
      'Pet Care'
    ];
    this.createService = this.createService.bind(this);
  }

  createService(serviceData) {
    actions.createService(serviceData).then(
      service => {
        this.setState({ redirect: true });
      },
      errors => this.setState({ errors })
    );
  }

  render() {
    const { errors, redirect } = this.state;

    if (redirect) {
      return <Redirect to={{ pathname: '/services' }} />;
    }

    return (
      <section id='newService'>
        <div className=' container py-4 ehe-form'>
          <div className='row'>
            <div className='col-md-5'>
              <h1 className='page-title'>Create Service</h1>
              <ServiceCreateForm
                submitCb={this.createService}
                options={this.serviceCategories}
                errors={errors}
              />
            </div>
            <div className='col-md-6 ml-auto'>
              <div className='image-container'>
                <h2 className='catchphrase'>Reach thousands of homeowners!</h2>
                <img
                  src={process.env.PUBLIC_URL + '/img/create-service.jpg'}
                  alt='create service'
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
