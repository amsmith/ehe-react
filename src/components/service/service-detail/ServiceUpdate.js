import React, { Component } from 'react';
import { connect } from 'react-redux';
import ServiceMap from './ServiceMap';
import Booking from '../../booking/Booking';
import UserGuard from '../../shared/auth/UserGuard';
import ServiceFeatures from './ServiceFeatures';
import { toUpperCase } from '../../../helpers';

import EditableInput from '../../shared/editable/EditableInput';
import EditableText from '../../shared/editable/EditableText';
import EditableSelect from '../../shared/editable/EditableSelect';
import EditableImage from '../../shared/editable/EditableImage';

import * as actions from '../../../actions';

class ServiceUpdate extends Component {
  constructor() {
    super();

    this.state = {
      isAllowed: false,
      isFetching: true
    };
    this.updateService = this.updateService.bind(this);
    this.resetServiceErrors = this.resetServiceErrors.bind(this);
    this.verifyServiceOwner = this.verifyServiceOwner.bind(this);
  }

  componentWillMount() {
    const serviceId = this.props.match.params.id;
    this.props.dispatch(actions.fetchServiceById(serviceId));
  }

  componentDidMount() {
    this.verifyServiceOwner();
  }

  updateService(serviceData) {
    const {
      service: { _id },
      dispatch
    } = this.props;
    dispatch(actions.updateService(_id, serviceData));
  }

  resetServiceErrors() {
    this.props.dispatch(actions.resetServiceErrors());
  }

  verifyServiceOwner() {
    const serviceId = this.props.match.params.id;

    this.setState({ isFetching: true });

    return actions
      .verifyServiceOwner(serviceId)
      .then(
        () => this.setState({ isAllowed: true, isFetching: false }),
        () => this.setState({ isAllowed: false, isFetching: false })
      );
  }

  render() {
    const { service, errors } = this.props;
    const { isAllowed, isFetching } = this.state;

    if (service._id) {
      return (
        <UserGuard isAllowed={isAllowed} isFetching={isFetching}>
          <section id='serviceDetails' className='container py-4'>
            <div className='upper-section'>
              <div className='row'>
                <div className='col-md-6'>
                  <EditableImage
                    entity={service}
                    entityField={'image'}
                    errors={errors}
                    updateEntity={this.updateService}
                  />
                </div>
                <div className='col-md-6'>
                  <ServiceMap location={`${service.city}, ${service.state}`} />
                </div>
              </div>
            </div>
            <div className='details-section'>
              <div className='row'>
                <div className='col-md-8'>
                  <div className='service'>
                    <EditableSelect
                      entity={service}
                      entityField={'category'}
                      updateEntity={this.updateService}
                      className={`service-type ${service.category}`}
                      options={[
                        'Landscaping',
                        'Housekeeping',
                        'Pet Care',
                        'Auto Detailing'
                      ]}
                      errors={errors}
                      resetErrors={this.resetServiceErrors}
                    />
                    <div className='service-owner'>
                      <img
                        src='https://api.adorable.io/avatars/285/abott@adorable.png'
                        alt='owner'
                      />
                      <span>{service.user && service.user.username}</span>
                    </div>

                    <EditableInput
                      entity={service}
                      entityField={'company'}
                      updateEntity={this.updateService}
                      className={'service-company'}
                      errors={errors}
                      resetErrors={this.resetServiceErrors}
                    />
                    <EditableInput
                      entity={service}
                      entityField={'city'}
                      updateEntity={this.updateService}
                      className={'service-city'}
                      errors={errors}
                      resetErrors={this.resetServiceErrors}
                      formatPipe={[toUpperCase]}
                    />
                    <div className='service-info'>
                      <span>
                        <i className='fa fa-star' />
                        {service.rating} Star Rating
                      </span>
                      <span>
                        <i className='fa fa-user' /> {service.jobsCompleted}{' '}
                        Jobs Completed
                      </span>
                      <span>
                        <i className='fa fa-dollar' />
                        <EditableInput
                          entity={service}
                          entityField={'price'}
                          updateEntity={this.updateService}
                          className={'service-price'}
                          containerStyle={{ display: 'inline-block' }}
                          errors={errors}
                          resetErrors={this.resetServiceErrors}
                        />
                      </span>
                    </div>
                    <EditableText
                      entity={service}
                      entityField={'description'}
                      updateEntity={this.updateService}
                      className={'service-description'}
                      rows={6}
                      cols={50}
                      errors={errors}
                      resetErrors={this.resetServiceErrors}
                    />
                    <hr />
                    <ServiceFeatures />
                  </div>
                </div>
                <div className='col-md-4'>
                  <Booking service={service} />
                </div>
              </div>
            </div>
          </section>
        </UserGuard>
      );
    } else {
      return <h1>Loading...</h1>;
    }
  }
}

const mapStateToProps = state => ({
  service: state.service.data,
  errors: state.service.errors
});

export default connect(mapStateToProps)(ServiceUpdate);
