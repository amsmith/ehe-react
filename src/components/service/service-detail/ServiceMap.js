import React, { Component } from 'react';
import { MapWithGeocode } from '../../map/GoogleMap';
import { connect } from 'react-redux';

import * as actions from '../../../actions';

class ServiceMap extends Component {
  constructor() {
    super();

    this.reloadMapFinish = this.reloadMapFinish.bind(this);
  }

  reloadMapFinish() {
    this.props.dispatch(actions.reloadMapFinish());
  }

  render() {
    const {
      location,
      map: { isReloading }
    } = this.props;

    return (
      <MapWithGeocode
        googleMapURL='https://maps.googleapis.com/maps/api/js?key=AIzaSyB0Ab4lmaAt0U-ADxhHw-fmxaPaU4z82cI&libraries=geometry,drawing,places'
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `405px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        location={location}
        isReloading={isReloading}
        mapLoaded={this.reloadMapFinish}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    map: state.map
  };
};

export default connect(mapStateToProps)(ServiceMap);
