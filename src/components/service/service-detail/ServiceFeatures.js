import React from "react";

const ServiceFeatures = () => {
  return (
    <div className="service-assets">
      <h3 className="title">Features</h3>
      <div className="row">
        <div className="col-md-6">
          <span>
            <i className="fa fa-calendar" /> Same-day Scheduling
          </span>
          <span>
            <i className="fa fa-certificate" /> Satisfaction Guarantee
          </span>
          <span>
            <i className="fa fa-location-arrow" /> Arrival Notification
          </span>
        </div>
        <div className="col-md-6">
          <span>
            <i className="fa fa-money" /> Price-Match
          </span>
          <span>
            <i className="fa fa-credit-card" /> Contract Discount
          </span>
          <span>
            <i className="fa fa-users" /> Referral Bonus
          </span>
        </div>
      </div>
    </div>
  );
};

export default ServiceFeatures;
