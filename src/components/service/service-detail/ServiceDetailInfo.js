import React from "react";
import ServiceFeatures from "./ServiceFeatures";
import { serviceType, toUpperCase } from "../../../helpers";

const ServiceDetailInfo = props => {
  const service = props.service;
  return (
    <div className="service">
      <h2 className={`service-type ${service.category}`}>
        {serviceType(service.recommended)}
        {service.category}
      </h2>
      <div className="service-owner">
        <img
          src="https://api.adorable.io/avatars/285/abott@adorable.png"
          alt="owner"
        />
        <span>{service.user && service.user.username}</span>
      </div>
      <h1 className="service-company">{service.company}</h1>
      <h2 className="service-city">{toUpperCase(service.city)}</h2>
      <div className="service-info">
        <span>
          <i className="fa fa-star" />
          {service.rating} Star Rating
        </span>
        <span>
          <i className="fa fa-user" /> {service.jobsCompleted} Jobs Completed
        </span>
        <span>
          <i className="fa fa-dollar" />
          <strong>{service.price}</strong>
        </span>
      </div>
      <p className="service-description">{service.description}</p>
      <hr />
      <ServiceFeatures />
    </div>
  );
};

export default ServiceDetailInfo;
