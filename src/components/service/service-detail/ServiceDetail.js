import React, { Component } from 'react';
import { connect } from 'react-redux';
import StarRatings from 'react-star-ratings';
import ServiceDetailInfo from './ServiceDetailInfo';
import ServiceMap from './ServiceMap';
import Booking from '../../booking/Booking';

import * as actions from '../../../actions';

class ServiceDetail extends Component {
  state = {
    reviews: []
  };

  componentWillMount() {
    const serviceId = this.props.match.params.id;
    this.props
      .dispatch(actions.fetchServiceById(serviceId))
      .then(service => this.getReviews(service._id));
  }

  getReviews = serviceId => {
    actions.getReviews(serviceId).then(reviews => {
      this.setState({ reviews });
    });
  };

  render() {
    const { service } = this.props;
    const { reviews } = this.state;

    if (service._id) {
      return (
        <section id='serviceDetails' className='container mt-3'>
          <div className='upper-section'>
            <div className='row'>
              <div className='col-md-6'>
                <img src={service.image} alt='' />
              </div>
              <div className='col-md-6'>
                <ServiceMap location={`${service.city}, ${service.state}`} />
              </div>
            </div>
          </div>
          <div className='details-section'>
            <div className='row'>
              <div className='col-md-8'>
                <ServiceDetailInfo service={service} />
              </div>
              <div className='col-md-4'>
                <Booking service={service} />
              </div>
            </div>
            {reviews && reviews.length > 0 && (
              <div className='row'>
                <div className='col-md-8'>
                  <section style={{ marginBottom: '40px' }}>
                    <h2>Reviews</h2>
                    {reviews.map(review => (
                      <div key={review._id} className='card review-card'>
                        <div className='card-body'>
                          <div className='row'>
                            <div className='col-md-2 user-image'>
                              <img
                                src='https://image.ibb.co/jw55Ex/def_face.jpg'
                                className='img img-rounded img-fluid'
                              />
                              <p className='text-secondary text-center'>
                                {review.createdAt}
                              </p>
                            </div>
                            <div className='col-md-10'>
                              <div>
                                <a>
                                  <strong>{review.user.username}</strong>
                                </a>
                                <div className='review-section'>
                                  <StarRatings
                                    rating={review.rating}
                                    starRatedColor='orange'
                                    starHoverColor='orange'
                                    starDimension='25px'
                                    starSpacing='2px'
                                    numberOfStars={5}
                                    name='rating'
                                  />
                                </div>
                              </div>
                              <div className='clearfix' />
                              <p>{review.text}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </section>
                </div>
              </div>
            )}
          </div>
        </section>
      );
    } else {
      return <h1>Loading...</h1>;
    }
  }
}

const mapStateToProps = state => ({
  service: state.service.data,
  errors: state.service.errors
});

export default connect(mapStateToProps)(ServiceDetail);
