import React, { Component } from 'react';
import ServiceList from './ServiceList';
import { connect } from 'react-redux';

import { toUpperCase } from '../../../helpers';
import * as actions from '../../../actions';

class ServiceSearchListing extends Component {
  constructor() {
    super();

    this.state = {
      searchedCity: ''
    };
  }
  componentWillMount() {
    this.searchServicesByCity();
  }

  componentDidUpdate(prevProps) {
    const currentUrlParam = this.props.match.params.city;
    const prevUrlParam = prevProps.match.params.city;
    if (currentUrlParam !== prevUrlParam) {
      this.searchServicesByCity();
    }
  }

  searchServicesByCity() {
    const searchedCity = this.props.match.params.city;
    this.setState({ searchedCity });

    this.props.dispatch(actions.fetchServices(searchedCity));
  }

  renderTitle() {
    const { errors, data } = this.props.services;
    const { searchedCity } = this.state;
    let title = '';

    if (errors.length > 0) {
      title = errors[0].detail;
    }

    if (data.length > 0) {
      title = `Services Available to Book in ${toUpperCase(searchedCity)}`;
    }
    return <h1 className='page-title'>{title}</h1>;
  }

  render() {
    return (
      <section id='serviceListing' className='container'>
        {this.renderTitle()}
        <ServiceList services={this.props.services.data} />
      </section>
    );
  }
}

const mapStateToProps = state => ({ services: state.services });

export default connect(mapStateToProps)(ServiceSearchListing);
