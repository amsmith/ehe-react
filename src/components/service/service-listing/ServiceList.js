import React, { Component } from "react";
import ServiceCard from "./ServiceCard";

class ServiceList extends Component {
  renderServices() {
    return this.props.services.map((service, index) => {
      return <ServiceCard key={index} service={service} />;
    });
  }

  render() {
    return <div className="row">{this.renderServices()}</div>;
  }
}

export default ServiceList;
