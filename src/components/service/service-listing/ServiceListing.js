import React, { Component } from 'react';
import ServiceList from './ServiceList';
import { connect } from 'react-redux';

import * as actions from '../../../actions';

class ServiceListing extends Component {
  componentWillMount() {
    this.props.dispatch(actions.fetchServices());
  }

  render() {
    return (
      <section className='container' id='serviceListing'>
        <ServiceList services={this.props.services} />
      </section>
    );
  }
}

const mapStateToProps = state => ({
  services: state.services.data
});

export default connect(mapStateToProps)(ServiceListing);
