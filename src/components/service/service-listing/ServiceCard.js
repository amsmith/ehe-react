import React from 'react';
import { Link } from 'react-router-dom';

import { serviceType } from '../../../helpers';

const ServiceCard = props => {
  const service = props.service;
  return (
    <div className='col-sm-6 col-md-4 mb-3  '>
      <Link to={`/services/${service._id}`} className='card-link'>
        <div className='card ehe-card'>
          <img className='card-img-top' src={service.image} alt='service' />
          <div className='card-body'>
            <h6 className={`card-subtitle ${service.category}`}>
              {serviceType(service.recommended)}
              {service.category}
            </h6>
            <h4 className='card-title'>{service.company}</h4>
            <p className='card-text'>{service.description}</p>
            <p className='card-text'>
              {service.rating} Rating &#183; {service.jobsCompleted} Jobs
              Completed
            </p>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ServiceCard;
