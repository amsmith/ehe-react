import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class ServiceSearchInput extends Component {
  constructor() {
    super();

    this.searchInput = React.createRef();
  }

  handleKeyPress(event) {
    if (event.key === "Enter") {
      this.handleSearch();
    }
  }

  handleSearch() {
    const { history } = this.props;
    const city = this.searchInput.current.value;

    city ? history.push(`/services/${city}/experts`) : history.push("/experts");
  }

  render() {
    return (
      <div className="form-inline my-2 my-lg-0">
        <input
          onKeyPress={event => {
            this.handleKeyPress(event);
          }}
          ref={this.searchInput}
          className="form-control mr-sm-2 ehe-search"
          type="search"
          placeholder="Search by City"
          aria-label="Search"
        />
        <button
          onClick={() => {
            this.handleSearch();
          }}
          className="btn btn-outline-light my-2 my-sm-0 btn-ehe-search"
          type="submit"
        >
          Search
        </button>
      </div>
    );
  }
}

export default withRouter(ServiceSearchInput);
