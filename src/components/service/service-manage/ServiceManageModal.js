import React, { Component } from "react";
import Modal from "react-responsive-modal";
import { prettifyDate } from "../../../helpers";

export default class RentalManageModal extends Component {
  constructor() {
    super();

    this.state = {
      open: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal() {
    this.setState({ open: true });
  }

  closeModal() {
    this.setState({ open: false });
  }

  renderBookings(bookings) {
    return bookings.map((booking, index) => (
      <React.Fragment key={index}>
        <p>
          <span>Date:</span> {prettifyDate(booking.startAt)} -
          {prettifyDate(booking.endAt)}
        </p>
        {index + 1 !== bookings.length && <hr />}
      </React.Fragment>
    ));
  }

  render() {
    const { bookings } = this.props;
    return (
      <React.Fragment>
        <button type="button" onClick={this.openModal} className="btn btn-ehe">
          Bookings
        </button>
        <Modal
          open={this.state.open}
          onClose={this.closeModal}
          little
          classNames={{ modal: "service-booking-modal" }}
        >
          <h4 className="modal-title title">Bookings</h4>
          <div className="modal-body bookings-inner-container">
            {this.renderBookings(bookings)}
          </div>
          <div className="modal-footer">
            <button
              type="button"
              onClick={this.closeModal}
              className="btn btn-ehe"
            >
              Cancel
            </button>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}
