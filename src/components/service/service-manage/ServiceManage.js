import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ServiceManageCard from './ServiceManageCard';
import ServiceManageModal from './ServiceManageModal';
import * as actions from '../../../actions';
import { ToastContainer, toast } from 'react-toastify';

class ServiceManage extends Component {
  constructor() {
    super();

    this.state = {
      userServices: [],
      errors: [],
      isFetching: false
    };

    this.deleteService = this.deleteService.bind(this);
  }

  componentWillMount() {
    this.setState({ isFetching: true });

    actions
      .getUserServices()
      .then(
        userServices => this.setState({ userServices, isFetching: false }),
        errors => this.setState({ errors, isFetching: false })
      );
  }

  renderServiceCards(services) {
    return services.map((service, index) => (
      <ServiceManageCard
        modal={<ServiceManageModal bookings={service.bookings} />}
        key={index}
        service={service}
        serviceIndex={index}
        deleteServiceCb={this.deleteService}
      />
    ));
  }

  deleteService(serviceId, serviceIndex) {
    actions.deleteService(serviceId).then(
      () => {
        this.deleteServiceFromList(serviceIndex);
      },
      errors => {
        toast.error(errors[0].detail);
      }
    );
  }

  deleteServiceFromList(serviceIndex) {
    const userServices = this.state.userServices.slice();

    userServices.splice(serviceIndex, 1);

    this.setState({ userServices });
  }

  render() {
    const { userServices, isFetching } = this.state;
    return (
      <section id='userServices' className='container py-4'>
        <ToastContainer />
        <h1 className='page-title'>My Services</h1>
        <div className='row'>{this.renderServiceCards(userServices)}</div>
        {!isFetching && userServices.length === 0 && (
          <div className='alert alert-warning'>
            You're not currently offering any Services. Register your Service
            now.
            <Link
              style={{ marginLeft: '10px' }}
              className='btn btn-ehe'
              to='/services/new'
            >
              Register Service
            </Link>
          </div>
        )}
      </section>
    );
  }
}
export default ServiceManage;
