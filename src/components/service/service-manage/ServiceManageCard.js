import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { toUpperCase, prettifyDate } from '../../../helpers';

class ServiceManageCard extends Component {
  constructor() {
    super();

    this.state = {
      wantDelete: false
    };
  }

  showDeleteMenu() {
    this.setState({
      wantDelete: true
    });
  }

  closeDeleteMenu() {
    this.setState({
      wantDelete: false
    });
  }

  deleteService(serviceId, serviceIndex) {
    this.setState({ wantDelete: false });

    this.props.deleteServiceCb(serviceId, serviceIndex);
  }

  render() {
    const { service, modal, serviceIndex } = this.props;
    const { wantDelete } = this.state;

    const deleteClass = wantDelete ? 'toBeDeleted' : '';

    return (
      <div className='col-md-4'>
        <div className={`card text-center mb-2 ${deleteClass}`}>
          <div className='card-block'>
            <h4 className='card-title'>
              {service.company} - {toUpperCase(service.city)}
            </h4>
            <Link className='btn btn-ehe' to={`/services/${service._id}`}>
              Go to Service
            </Link>
            {service.bookings && service.bookings.length > 0 && modal}
          </div>
          <div className='card-footer text-muted'>
            Created at {prettifyDate(service.createdAt)}
            {!wantDelete && (
              <React.Fragment>
                <button
                  onClick={() => this.showDeleteMenu()}
                  className='btn-sm btn-danger'
                >
                  Delete
                </button>
                <Link
                  className='btn-sm btn-warning'
                  to={{
                    pathname: `/services/${service._id}/edit`,
                    state: { isUpdate: true }
                  }}
                >
                  Edit
                </Link>
              </React.Fragment>
            )}
            {wantDelete && (
              <div className='delete-menu'>
                Are you sure?
                <button
                  onClick={() => {
                    this.deleteService(service._id, serviceIndex);
                  }}
                  className='btn-sm btn-danger'
                >
                  Yes
                </button>
                <button
                  onClick={() => {
                    this.closeDeleteMenu();
                  }}
                  className='btn-sm btn-success'
                >
                  No
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceManageCard;
