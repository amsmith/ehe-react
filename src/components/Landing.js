import React from 'react';
import providerPhoto from '../img/adult-beard-boy.jpg';
import aboutPhoto from '../img/about.jpg';

function Landing() {
  return (
    <React.Fragment>
      <section id='showcase'>
        <div className='container text-center'>
          <div className='home-search p-5'>
            <div className='overlay p-5'>
              <h1 className='display-3 mb-4'>Home Services Made Easy</h1>
              <p className='sub-title'>
                Hand-Picked Experts...Book in Minutes...Low Prices!!!
              </p>
            </div>
          </div>
        </div>
      </section>
      <section id='about' className='py-4'>
        <div className='container pb-5'>
          <div className='row'>
            <div className='col-md-8'>
              <h2>One-Stop Shopping for Home Services</h2>
              <p className='lead'>
                Landscaping, Maid Service, Pet Care, Auto Detailing and More!
              </p>
              <img className='w-100' src={aboutPhoto} alt='' />
              <p className='mt-4'>
                At Easy Home Expert, our mission is to transform the process of
                purchasing home services from a frustrating and time-consuming
                experience to an easy and enjoyable one. We've built a highly
                efficient online booking and payment platform and partnered with
                only the best hand-picked home service experts. By pooling the
                capacity of these dedicated small business owners and
                streamlining the purchasing process, we're able to deliver the
                highest quality services at lower prices. Enjoy life. Leave the
                work to us!
              </p>
            </div>
            <div className='col-md-4'>
              <div className='card'>
                <img
                  className='card-img-top'
                  src={providerPhoto}
                  alt='Provider of the month'
                />
                <div className='card-body'>
                  <h5 className='card-title'>Expert Of The Month</h5>
                  <h6 className='text-secondary'>Jason Jones</h6>
                  <p className='card-text'>
                    As the owner of Lawn Lubbers, Jason was one of the first
                    experts to join the Easy Home platform. We were able to book
                    23 jobs for Jason in the first month, for which Lawn Lubbers
                    received an average 4.9 Star Rating!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id='services' class='py-5 text-white'>
        <div className='container'>
          <div className='row text-center'>
            <div className='col-md-4'>
              <i className='fa fa-mobile fa-4x mr-4' />
              <hr />
              <h3>Book Online</h3>
              <p>
                Your days of playing phone tag, paper invoices, and mailing
                checks are over. Purchase on-demand or recurring home services
                in minutes.
              </p>
            </div>
            <div class='col-md-4'>
              <i class='fa fa-home fa-4x mr-4' />
              <hr />
              <h3>Satisfaction Guaranteed</h3>
              <p>
                We select only the best home experts who can meet and maintain
                our rigorous standards. You're not charged unless you're
                satisfied.
              </p>
            </div>
            <div class='col-md-4'>
              <i class='fa fa-dollar fa-4x mr-4' />
              <hr />
              <h3>Low Prices</h3>
              <p>
                Our digital platform allows us to work with high quality
                independent home services experts at scale, leveraging
                technology to keep prices low.
              </p>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
}

export default Landing;
