import React from "react";
import { Field, reduxForm } from "redux-form";
import Eheinput from "../shared/form/Eheinput";
import EheResErrors from "../shared/form/EheResErrors";

const RegisterForm = props => {
  const { handleSubmit, pristine, submitting, submitCb, valid, errors } = props;
  return (
    <form onSubmit={handleSubmit(submitCb)}>
      <Field
        name="username"
        type="text"
        label="Username"
        className="form-control"
        component={Eheinput}
      />
      <Field
        name="email"
        type="email"
        label="Email"
        className="form-control"
        component={Eheinput}
      />
      <Field
        name="password"
        type="password"
        label="Password"
        className="form-control"
        component={Eheinput}
      />
      <Field
        name="passwordConfirmation"
        type="password"
        label="Confirm Password"
        className="form-control"
        component={Eheinput}
      />
      <button
        className="btn btn-ehe btn-form"
        type="submit"
        disabled={!valid || pristine || submitting}
      >
        Register
      </button>
      <EheResErrors errors={errors} />
    </form>
  );
};

const validate = values => {
  const errors = {};
  if (!values.username || values.username.length < 4) {
    errors.username = "Username must be at least 4 characters.";
  }

  if (!values.email) {
    errors.email = "Valid email address is required.";
  }

  if (!values.password || values.password.length < 4) {
    errors.password = "Password must be at least 4 characters.";
  }

  if (
    !values.passwordConfirmation ||
    values.password !== values.passwordConfirmation
  ) {
    errors.passwordConfirmation = "Password confirmation must match password.";
  }

  return errors;
};

export default reduxForm({
  form: "registerForm",
  validate
})(RegisterForm);
