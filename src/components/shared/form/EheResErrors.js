import React from "react";

const EheResErrors = props => {
  const errors = props.errors;

  return (
    errors.length > 0 && (
      <div className="alert alert-danger ehe-res-errors">
        {errors.map((error, index) => (
          <p key={index}>{error.detail}</p>
        ))}
      </div>
    )
  );
};

export default EheResErrors;
