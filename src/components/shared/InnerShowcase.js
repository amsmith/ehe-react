import React from 'react';

function InnerShowcase() {
  return (
    <section id='showcase-inner' class='py-4 text-white'>
      <div class='container'>
        <div class='row text-center'>
          <div class='col-md-12'>
            <h1 class='display-4'>As Easy As 1..2..3</h1>
            <p class='lead'>
              Select Your Service...Book a Time...Satisfaction Guaranteed!
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}

export default InnerShowcase;
