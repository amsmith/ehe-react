import React from 'react';
import EditableComponent from './EditableComponent';
import EheFileUpload from '../form/EheFileUpload';

export default class EditableImage extends EditableComponent {
  handleImageUpload(image) {
    this.setState({ value: image });

    this.update();
  }

  render() {
    const { isActive, value } = this.state;

    return (
      <div className='editableComponent'>
        {!isActive && (
          <React.Fragment>
            <img src={value} alt='' />
            <button
              onClick={() => this.enableEdit()}
              type='button'
              className='btn-sm btn-warning btn-editable btn-editable-image'
            >
              Edit
            </button>
          </React.Fragment>
        )}

        {isActive && (
          <React.Fragment>
            <button
              onClick={() => this.disableEdit()}
              type='button'
              className='btn btn-warning btn-editable btn-editable-image'
            >
              Close
            </button>
            <EheFileUpload onChange={image => this.handleImageUpload(image)} />
          </React.Fragment>
        )}
      </div>
    );
  }
}
