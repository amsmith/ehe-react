import React from 'react';
import EditableComponent from './EditableComponent';

export default class EditableInput extends EditableComponent {
  formatView(value) {
    const { formatPipe } = this.props;

    if (formatPipe) {
      let formattedValue = value;

      formatPipe.forEach(pipe => (formattedValue = pipe(formattedValue)));
      return formattedValue;
    }
    return value;
  }

  renderComponentView() {
    const { value, isActive } = this.state;
    const { className } = this.props;

    if (isActive) {
      return (
        <React.Fragment>
          <input
            onChange={event => this.handleChange(event)}
            value={value}
            className={className}
          />
          <button
            onClick={() => this.disableEdit()}
            type='button'
            className='btn btn-warning btn-editable'
          >
            Close
          </button>
          <button
            onClick={() => this.update()}
            type='button'
            className='btn btn-success btn-editable'
          >
            Save
          </button>
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <span className={className}>{this.formatView(value)}</span>
        <button
          onClick={() => this.enableEdit()}
          type='button'
          className='btn-sm btn-warning btn-editable'
        >
          Edit
        </button>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className='editableComponent' style={this.props.containerStyle}>
        {this.renderComponentView()}
      </div>
    );
  }
}
