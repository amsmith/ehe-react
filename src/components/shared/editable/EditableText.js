import React from 'react';
import EditableComponent from './EditableComponent';

export default class EditableText extends EditableComponent {
  renderComponentView() {
    const { value, isActive } = this.state;
    const { className, rows, cols } = this.props;

    if (isActive) {
      return (
        <React.Fragment>
          <textarea
            onChange={event => this.handleChange(event)}
            value={value}
            className={className}
            rows={rows}
            cols={cols}
          />
          <button
            onClick={() => this.disableEdit()}
            type='button'
            className='btn btn-warning btn-editable'
          >
            Close
          </button>
          <button
            onClick={() => this.update()}
            type='button'
            className='btn btn-success btn-editable'
          >
            Save
          </button>
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <span className={className}>{value}</span>
        <button
          onClick={() => this.enableEdit()}
          type='button'
          className='btn-sm btn-warning btn-editable'
        >
          Edit
        </button>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className='editableComponent' style={this.props.containerStyle}>
        {this.renderComponentView()}
      </div>
    );
  }
}
