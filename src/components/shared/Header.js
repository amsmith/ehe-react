import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import ServiceSearchInput from '../service/ServiceSearchInput';

class Header extends Component {
  constructor() {
    super();
    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    this.props.logout();
    this.props.history.push('/services');
  }

  renderAuthButtons(isAuth) {
    if (isAuth) {
      return (
        <a className='nav-item nav-link clickable' onClick={this.handleLogout}>
          Log out
        </a>
      );
    } else {
      return (
        <React.Fragment>
          <Link className='nav-item nav-link' to='/login'>
            Log In <span className='sr-only'>(current)</span>
          </Link>
          <Link className='nav-item nav-link' to='/register'>
            Register
          </Link>
        </React.Fragment>
      );
    }
  }

  renderOwnerSection(isAuth) {
    if (isAuth) {
      return (
        <div className='nav-item dropdown'>
          <a
            className='nav-link nav-item dropdown-toggle clickable'
            id='navbarDropdownMenuLink'
            data-toggle='dropdown'
            aria-haspopup='true'
            aria-expanded='false'
          >
            Manage
          </a>
          <div
            className='dropdown-menu'
            aria-labelledby='navbarDropdownMenuLink'
          >
            <Link className='dropdown-item' to='/services/new'>
              Create Service
            </Link>
            <Link className='dropdown-item' to='/services/manage'>
              Manage Services
            </Link>
            <Link className='dropdown-item' to='/bookings/manage'>
              Manage Bookings
            </Link>
          </div>
        </div>
      );
    }
  }

  render() {
    const { username, isAuth } = this.props.auth;
    return (
      <section id='top-bar'>
        <div className='container p-2 top-bar_content'>
          <div className='row'>
            <div className='col-md-6'>
              <i className='fa fa-envelope-open' /> contact@easyhomeexpert.com
            </div>
            <div className='col-md-6'>
              <div className='social text-right'>
                <a href='https://twitter.com/EasyHomeExpert' target='_blank'>
                  <i className='fa fa-twitter' />
                </a>
                <a href='http://facebook.com' target='_blank'>
                  <i className='fa fa-facebook' />
                </a>
                <a
                  href='https://www.linkedin.com/company/easyhomeexpert/about/'
                  target='_blank'
                >
                  <i className='fa fa-linkedin' />
                </a>
                <a href='http://instagram.com' target='_blank'>
                  <i className='fa fa-instagram' />
                </a>
                <a
                  href='https://www.pinterest.com/easyhomeexpert'
                  target='_blank'
                >
                  <i className='fa fa-pinterest' />
                </a>
              </div>
            </div>
          </div>
        </div>
        <nav className='navbar navbar-dark navbar-expand-lg'>
          <div className='container'>
            <Link className='navbar-brand text-white' to='/'>
              Easy Home Expert
            </Link>
            <ServiceSearchInput />
            <button
              className='navbar-toggler'
              type='button'
              data-toggle='collapse'
              data-target='#navbarNavAltMarkup'
              aria-controls='navbarNavAltMarkup'
              aria-expanded='false'
              aria-label='Toggle navigation'
            >
              <span className='navbar-toggler-icon' />
            </button>
            <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>
              <div className='navbar-nav ml-auto'>
                <Link
                  className='nav-item nav-link mr-2 lead cta'
                  to='/services'
                >
                  BOOK NOW!
                </Link>
                {isAuth && <a className='nav-item nav-link'>{username}</a>}
                {this.renderOwnerSection(isAuth)}
                {this.renderAuthButtons(isAuth)}
              </div>
            </div>
          </div>
        </nav>
      </section>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default withRouter(connect(mapStateToProps)(Header));
