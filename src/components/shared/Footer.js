import React from 'react';

function Footer() {
  return (
    <footer id='main-footer' className='footer text-white text-center py-1'>
      Copyright &copy; <span className='year' /> Easy Home Expert
    </footer>
  );
}

export default Footer;
