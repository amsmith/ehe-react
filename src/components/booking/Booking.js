import React, { Component } from 'react';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { BookingModal } from './BookingModal';
import { getRangeOfDates } from '../../helpers';
import Payment from '../payment/Payment';

import * as moment from 'moment';
import * as actions from '../../actions';

class Booking extends Component {
  constructor() {
    super();

    this.bookedOutDates = [];
    this.dateRef = React.createRef();

    this.state = {
      proposedBooking: {
        startAt: '',
        endAt: '',
        notes: '',
        paymentToken: ''
      },
      modal: { open: false },
      errors: []
    };

    this.checkInvalidDates = this.checkInvalidDates.bind(this);
    this.handleApply = this.handleApply.bind(this);
    this.cancelConfirmation = this.cancelConfirmation.bind(this);
    this.reserveService = this.reserveService.bind(this);
    this.setPaymentToken = this.setPaymentToken.bind(this);
  }

  componentWillMount() {
    this.getBookedOutDates();
  }

  getBookedOutDates() {
    const { bookings } = this.props.service;

    if (bookings && bookings.length > 0) {
      bookings.forEach(booking => {
        const dateRange = getRangeOfDates(
          booking.startAt,
          booking.endAt,
          'Y/MM/DD'
        );
        this.bookedOutDates.push(...dateRange);
      });
    }
  }

  checkInvalidDates(date) {
    // return (
    //   this.bookedOutDates.includes(date.format('Y/MM/DD')) ||
    //   date.diff(moment(), 'days') < 0
    // );
  }

  handleApply(event, picker) {
    const startAt = picker.startDate.format('Y/MM/DD');
    const endAt = picker.endDate.format('Y/MM/DD');

    this.dateRef.current.value = `${startAt} to ${endAt}`;

    this.setState({
      proposedBooking: {
        ...this.state.proposedBooking,
        startAt,
        endAt
      }
    });
  }

  selectNotes(event) {
    this.setState({
      proposedBooking: {
        ...this.state.proposedBooking,
        notes: String(event.target.value)
      }
    });
  }

  cancelConfirmation() {
    this.setState({
      modal: {
        open: false
      }
    });
  }

  setPaymentToken(paymentToken) {
    const { proposedBooking } = this.state;
    proposedBooking.paymentToken = paymentToken;
    this.setState({ proposedBooking });
  }

  addNewBookedOutDates(booking) {
    const dateRange = getRangeOfDates(booking.startAt, booking.endAt);
    this.bookedOutDates.push(...dateRange);
  }

  resetData() {
    this.dateRef.current.value = '';
    this.setState({ proposedBooking: { notes: '' } });
  }

  confirmProposedData() {
    const { service } = this.props;

    this.setState({
      proposedBooking: {
        ...this.state.proposedBooking,
        totalPrice: service.price,
        service
      },
      modal: {
        open: true
      }
    });
    console.log(this.state);
  }

  reserveService() {
    actions.createBooking(this.state.proposedBooking).then(
      booking => {
        this.addNewBookedOutDates(booking);
        this.cancelConfirmation();
        this.resetData();
        toast.success('Your service was successfully booked!');
      },
      errors => {
        this.setState({ errors });
      }
    );
  }

  render() {
    const {
      service,
      auth: { isAuth }
    } = this.props;
    const { startAt, endAt, notes, paymentToken } = this.state.proposedBooking;

    return (
      <div className='booking'>
        <h3 className='booking-price'>
          $ {service.price} <span className='booking-per-job'>per job</span>
        </h3>
        <hr />
        {!isAuth && (
          <Link
            className='btn btn-ehe btn-confirm btn-block'
            to={{ pathname: '/login' }}
          >
            Log in to Book Service
          </Link>
        )}
        {isAuth && (
          <React.Fragment>
            <div className='form-group'>
              <label htmlFor='dates'>Dates</label>
              <DateRangePicker
                onApply={this.handleApply}
                isInvalidDate={this.checkInvalidDates}
                opens='left'
                containerStyles={{ display: 'block' }}
              >
                <input
                  ref={this.dateRef}
                  id='dates'
                  type='text'
                  className='form-control'
                />
              </DateRangePicker>
            </div>
            <div className='form-group'>
              <label htmlFor='notes'>Instructions for Provider</label>
              <input
                onChange={event => this.selectNotes(event)}
                value={notes}
                type='text'
                className='form-control'
                id='notes'
                aria-describedby='emailHelp'
                placeholder=''
              />
            </div>
            <button
              disabled={!startAt || !endAt}
              onClick={() => this.confirmProposedData()}
              className='btn btn-ehe btn-confirm btn-block'
            >
              Book Now
            </button>
          </React.Fragment>
        )}
        <hr />
        <p className='booking-note-title'>One of the top-rated Home Experts</p>
        <p className='booking-note-text'>
          More than 100 people selected this Expert in 2018.
        </p>
        <BookingModal
          open={this.state.modal.open}
          closeModal={this.cancelConfirmation}
          confirmModal={this.reserveService}
          booking={this.state.proposedBooking}
          errors={this.state.errors}
          disabled={!paymentToken}
          acceptPayment={() => (
            <Payment setPaymentToken={this.setPaymentToken} />
          )}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(Booking);
