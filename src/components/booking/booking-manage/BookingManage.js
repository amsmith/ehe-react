import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { BookingCard, PaymentCard } from './BookingCard';
import ReviewModal from '../../review/ReviewModal';
import { isExpired } from '../../../helpers';

import * as actions from '../../../actions';

class BookingManage extends Component {
  state = {
    pendingPayments: []
  };

  componentDidMount() {
    this.props.dispatch(actions.fetchUserBookings());
    this.getPendingPayments();
  }

  getPendingPayments() {
    actions
      .getPendingPayments()
      .then(pendingPayments => this.setState({ pendingPayments }))
      .catch(err => console.error(err));
  }

  acceptPayment(payment) {
    actions
      .acceptPayment(payment)
      .then(status => {
        this.getPendingPayments();
      })
      .catch(err => console.error(err));
  }

  declinePayment(payment) {
    actions
      .declinePayment(payment)
      .then(status => {
        this.getPendingPayments();
      })
      .catch(err => console.error(err));
  }

  handleReviewCreated = (review, bookingIndex) => {
    const { dispatch } = this.props;
    const { data: bookings } = this.props.userBookings;

    bookings[bookingIndex].review = review;

    dispatch(actions.updateBookings(bookings));
  };

  renderBookings(bookings) {
    return bookings.map((booking, index) => (
      <BookingCard
        booking={booking}
        key={index}
        hasReview={!!booking.review}
        isExpired={isExpired(booking.endAt)}
        reviewModal={() => (
          <ReviewModal
            onReviewCreated={review => {
              this.handleReviewCreated(review, index);
            }}
            bookingId={booking._id}
          />
        )}
      />
    ));
  }

  renderPayments(payments) {
    return payments.map((payment, index) => (
      <PaymentCard
        booking={payment.booking}
        payment={payment}
        paymentBtns={this.renderPaymentButtons}
        key={index}
      />
    ));
  }

  renderPaymentButtons = payment => {
    return (
      <div>
        <button
          onClick={() => this.acceptPayment(payment)}
          className='btn btn-success'
        >
          Accept
        </button>{' '}
        <button
          onClick={() => this.declinePayment(payment)}
          className='btn btn-danger'
        >
          Decline
        </button>
      </div>
    );
  };

  render() {
    const { data: bookings, isFetching } = this.props.userBookings;
    const { pendingPayments } = this.state;

    return (
      <React.Fragment>
        <section id='userBookings' className='container py-4'>
          <h1 className='page-title'>Scheduled Services</h1>
          <div className='row'>{this.renderBookings(bookings)}</div>
          {!isFetching && bookings.length === 0 && (
            <div className='alert alert-warning'>
              You have not booked any Services.
              <Link
                style={{ marginLeft: '10px' }}
                className='btn btn-ehe ml-3'
                to='/services'
              >
                Book Service
              </Link>
            </div>
          )}
        </section>
        <section id='pendingBookings' className='container py-4'>
          <h1 className='page-title'>Pending Payments</h1>
          <div className='row'>{this.renderPayments(pendingPayments)}</div>
          {!isFetching && pendingPayments.length === 0 && (
            <div className='alert alert-warning'>
              You currently have no payments pending.
            </div>
          )}
        </section>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  userBookings: state.userBookings
});

export default connect(mapStateToProps)(BookingManage);
