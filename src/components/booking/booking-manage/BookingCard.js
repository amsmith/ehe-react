import React from 'react';

import { Link } from 'react-router-dom';

import { toUpperCase, prettifyDate } from '../../../helpers';

export function BookingCard(props) {
  const { booking, reviewModal, hasReview, isExpired } = props;
  return (
    <div className='col-md-4 mb-2'>
      <div className='card text-center'>
        <div className='card-header'>
          {booking.service
            ? booking.service.category
            : 'Service No Longer Exists'}
        </div>
        <div className='card-block py-2'>
          {booking.service && (
            <div>
              <h4 className='card-title'>
                {booking.service.company} - {toUpperCase(booking.service.city)}
              </h4>
              <p className='card-text booking-desc'>
                {booking.service.description}
              </p>
              <p className='card-text booking-notes'>{booking.service.notes}</p>
            </div>
          )}
          <p className='card-text booking-days'>
            {prettifyDate(booking.startAt)} - {prettifyDate(booking.endAt)}
          </p>
          <p className='card-text booking-price'>
            <span>Payment: </span>{' '}
            <span className='booking-price-value'>
              ${booking.service.price}
            </span>
          </p>
          {booking.service && (
            <Link
              className='btn btn-ehe'
              to={`/services/${booking.service._id}`}
            >
              Go to Service
            </Link>
          )}
          {reviewModal && isExpired && !hasReview && reviewModal()}
        </div>
        <div className='card-footer text-muted'>
          Created {prettifyDate(booking.createdAt)}
        </div>
      </div>
    </div>
  );
}

export function PaymentCard(props) {
  const { booking, payment, paymentBtns } = props;
  return (
    <div className='col-md-4 mb-2'>
      <div className='card text-center'>
        <div className='card-header'>
          Booking Made By {payment.fromUser.username}
        </div>
        <div className='card-block py-2'>
          {booking.service && (
            <div>
              <h4 className='card-title'>
                {booking.service.company} - {toUpperCase(booking.service.city)}
              </h4>
              <p className='card-text booking-desc'>
                {booking.service.description}
              </p>
              <p className='card-text booking-notes'>{booking.service.notes}</p>
            </div>
          )}
          <p className='card-text booking-days'>
            {prettifyDate(booking.startAt)} - {prettifyDate(booking.endAt)}
          </p>
          <p className='card-text booking-price'>
            <span>Payment Amount: </span>{' '}
            <span className='booking-price-value'>${payment.amount / 100}</span>
          </p>
          <p className='card-text payment-status'>Status: {payment.status}</p>
          {booking.service && (
            <Link
              className='btn btn-ehe'
              to={`/services/${booking.service._id}`}
            >
              Go to Service
            </Link>
          )}
        </div>
        <div className='card-footer text-muted'>
          Created {prettifyDate(booking.createdAt)}
          {payment.status === 'pending' && paymentBtns && paymentBtns(payment)}
        </div>
      </div>
    </div>
  );
}
