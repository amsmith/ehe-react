import React from 'react';
import Modal from 'react-responsive-modal';
import EheResErrors from '../shared/form/EheResErrors';

export function BookingModal(props) {
  const {
    open,
    closeModal,
    booking,
    confirmModal,
    errors,
    acceptPayment,
    disabled
  } = props;
  return (
    <Modal
      open={open}
      onClose={closeModal}
      little
      classNames={{ modal: 'booking-modal' }}
    >
      <h4 className='modal-title title'>Confirm Booking </h4>
      <p className='dates'>
        {booking.startAt} to {booking.endAt}
      </p>
      <div className='modal-body'>
        <p>
          Price: <em>${booking.totalPrice} </em>
        </p>
        <p>
          Instructions: <em>{booking.notes}</em>
        </p>
        {acceptPayment && acceptPayment()}
        <p>Confirm booking for selected service?</p>
      </div>
      <EheResErrors errors={errors} />
      <div className='modal-footer'>
        <button
          disabled={disabled}
          onClick={confirmModal}
          type='button'
          className='btn btn-ehe'
        >
          Book It!
        </button>
        <button type='button' onClick={closeModal} className='btn btn-ehe'>
          Cancel
        </button>
      </div>
    </Modal>
  );
}
