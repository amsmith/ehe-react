const User = require("../models/user");
const { normalizeErrors } = require("../helpers/mongoose");
const jwt = require("jsonwebtoken");
const config = require("../config");

exports.auth = function(req, res) {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).send({
      errors: [
        { title: "Missing data", detail: "Email and Password required." }
      ]
    });
  }

  User.findOne({ email }, (err, user) => {
    if (err) {
      return res.status(422).send({ errors: normalizeErrors(err.errors) });
    }

    if (!user) {
      return res.status(422).send({
        errors: [{ title: "Invalid User", detail: "User does not exist" }]
      });
    }

    if (user.hasSamePassword(password)) {
      const token = jwt.sign(
        {
          userId: user.id,
          username: user.username,
          password: user.password
        },
        config.SECRET,
        { expiresIn: "1h" }
      );
      return res.json(token);
    } else {
      return res.status(422).send({
        errors: [
          {
            title: "Wrong Password",
            detail: "Incorrect Password"
          }
        ]
      });
    }
  });
};

exports.register = function(req, res) {
  const { username, email, password, passwordConfirmation } = req.body;

  if (!email || !email) {
    return res.status(422).send({
      errors: [
        { title: "Missing data", detail: "Email and Password required." }
      ]
    });
  }

  if (password !== passwordConfirmation) {
    return res.status(422).send({
      errors: [
        {
          title: "Mismatched password",
          detail: "Password and confirmation values are not the same."
        }
      ]
    });
  }

  User.findOne({ email }, (err, existingUser) => {
    if (err) {
      return res.status(422).send({ errors: normalizeErrors(err.errors) });
    }

    if (existingUser) {
      return res.status(422).send({
        errors: [
          {
            title: "Email taken",
            detail: "This email address is already registered."
          }
        ]
      });
    }

    const user = new User({
      username,
      email,
      password
    });

    user.save(err => {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      return res.json({ registered: true });
    });
  });
};

exports.authMiddleware = (req, res, next) => {
  const token = req.headers.authorization;

  if (token) {
    const user = parseToken(token);

    User.findById(user.userId, (err, user) => {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      if (user) {
        res.locals.user = user;
        next();
      } else {
        return notAuthorized(res);
      }
    });
  } else {
    return notAuthorized(res);
  }
};

const parseToken = token => {
  return jwt.verify(token.split(" ")[1], config.SECRET);
};

const notAuthorized = res => {
  return res.status(401).send({
    errors: [
      {
        title: "Not authorized",
        detail: "Log in with valid credentials."
      }
    ]
  });
};
