const User = require('../models/user');
const Service = require('../models/service');
const Review = require('../models/review');
const Booking = require('../models/booking');
const moment = require('moment');
const { normalizeErrors } = require('../helpers/mongoose');

exports.getReviews = function(req, res) {
  const { serviceId } = req.query;

  Review.find({ service: serviceId })
    .populate('user')
    .exec((err, reviews) => {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      return res.json(reviews);
    });
};

exports.createReview = function(req, res) {
  const reviewData = req.body;
  const { bookingId } = req.query;
  const user = res.locals.user;

  Booking.findById(bookingId)
    .populate({ path: 'service', populate: { path: 'user' } })
    .populate('review')
    .populate('user')
    .exec(async (err, foundBooking) => {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      const { service } = foundBooking;

      if (service.user.id === user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid User',
              detail: 'Cannot review your own service.'
            }
          ]
        });
      }

      const foundBookingUserId = foundBooking.user.id;

      if (foundBookingUserId !== user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid Reviewer',
              detail: 'Can only review your own booking.'
            }
          ]
        });
      }

      const timeNow = moment();
      const endAt = moment(foundBooking.endAt);

      if (!endAt.isBefore(timeNow)) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid Date',
              detail: 'Cannot review until service has been performed.'
            }
          ]
        });
      }

      if (foundBooking.review) {
        return res.status(422).send({
          errors: [
            {
              title: 'Duplicate review',
              detail: 'Only one review per booking is allowed.'
            }
          ]
        });
      }

      const review = new Review(reviewData);
      review.user = user;
      review.service = service;
      foundBooking.review = review;

      try {
        await foundBooking.save();
        const savedReview = await review.save();
        return res.json(savedReview);
      } catch (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }
    });
};
