const Booking = require('../models/booking');
const Service = require('../models/service');
const User = require('../models/user');
const Payment = require('../models/payment');
const { normalizeErrors } = require('../helpers/mongoose');
const moment = require('moment');
const config = require('../config');
const stripe = require('stripe')(config.STRIPE_SK);

const PROVIDER_SHARE = 0.9;

exports.createBooking = function(req, res) {
  const {
    startAt,
    endAt,
    totalPrice,
    notes,
    days,
    service,
    paymentToken
  } = req.body;
  const user = res.locals.user;

  const booking = new Booking({
    startAt,
    endAt,
    totalPrice,
    notes,
    days
  });

  Service.findById(service._id)
    .populate('bookings')
    .populate('user')
    .exec(async function(err, foundService) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      if (foundService.user.id === user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid User',
              detail: 'Cannot book your own service.'
            }
          ]
        });
      }

      if (isValidBooking(booking, foundService)) {
        booking.user = user;
        booking.service = foundService;
        foundService.bookings.push(booking);

        const { payment, err } = await createPayment(
          booking,
          foundService.user,
          paymentToken
        );

        if (payment) {
          booking.payment = payment;

          booking.save(function(err) {
            if (err) {
              return res
                .status(422)
                .send({ errors: normalizeErrors(err.errors) });
            }
            foundService.save();
            User.update(
              { _id: user.id },
              { $push: { bookings: booking } },
              function() {}
            );

            return res.json({ startAt: booking.startAt, endAt: booking.endAt });
          });
        } else {
          return res.status(422).send({
            errors: [
              {
                title: 'Payment Error',
                detail: err
              }
            ]
          });
        }
      } else {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid Booking',
              detail: 'This time slot is already taken.'
            }
          ]
        });
      }
    });
};

exports.getUserBookings = function(req, res) {
  const user = res.locals.user;

  Booking.where({ user })
    .populate('service')
    .exec(function(err, foundBookings) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }
      return res.json(foundBookings);
    });
};

function isValidBooking(proposedBooking, service) {
  let isValid = true;

  if (service.bookings && service.bookings.length > 0) {
    isValid = service.bookings.every(function(booking) {
      const proposedStart = moment(proposedBooking.startAt);
      const proposedEnd = moment(proposedBooking.endAt);

      const actualStart = moment(booking.startAt);
      const actualEnd = moment(booking.endAt);

      return (
        (actualStart < proposedStart && actualEnd < proposedStart) ||
        (proposedEnd < actualEnd && proposedEnd < actualStart)
      );
    });
  }
  return isValid;
}

async function createPayment(booking, toUser, token) {
  const { user } = booking;
  const tokenId = token.id || token;

  const customer = await stripe.customers.create({
    source: tokenId,
    email: user.email
  });

  if (customer) {
    User.update(
      { _id: user.id },
      { $set: { stripeCustomerId: customer.id } },
      () => {}
    );

    const payment = new Payment({
      fromUser: user,
      toUser,
      fromStripeCustomerId: customer.id,
      booking,
      tokenId: token.id,
      amount: booking.totalPrice * 100 * PROVIDER_SHARE
    });

    try {
      const savedPayment = await payment.save();
      return { payment: savedPayment };
    } catch (err) {
      return { err: err.message };
    }
  } else {
    return { err: 'Cannot process payment' };
  }
}
