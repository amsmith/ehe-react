const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bookingSchema = new Schema({
  startAt: { type: Date, required: 'Starting Date is Required' },
  endAt: { type: Date, required: 'Ending Date is Required' },
  totalPrice: Number,
  days: Number,
  notes: { type: String },
  createdAt: { type: Date, default: Date.now },
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  service: { type: Schema.Types.ObjectId, ref: 'Service' },
  payment: { type: Schema.Types.ObjectId, ref: 'Payment' },
  status: { type: String, default: 'pending' },
  review: { type: Schema.Types.ObjectId, ref: 'Review' }
});

module.exports = mongoose.model('Booking', bookingSchema);
