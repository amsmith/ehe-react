const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
  username: {
    type: String,
    min: [4, 'Username must be at least 4 characters.'],
    max: [32, 'Username must be less than 33 characters.']
  },
  email: {
    type: String,
    required: 'Email is required.',
    unique: true,
    min: [4, 'Email must be at least 4 characters.'],
    max: [32, 'Email must be less than 33 characters.'],
    lowercase: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please provide a valid email address.'
    ]
  },
  password: {
    type: String,
    required: 'Password is required.',
    min: [4, 'Password must be at least 4 characters.'],
    max: [32, 'Password must be less than 33 characters.']
  },
  stripeCustomerId: String,
  revenue: Number,
  services: [{ type: Schema.Types.ObjectId, ref: 'Service' }],
  bookings: [{ type: Schema.Types.ObjectId, ref: 'Booking' }]
});

userSchema.methods.hasSamePassword = function(requestedPassword) {
  return bcrypt.compareSync(requestedPassword, this.password);
};

userSchema.pre('save', function(next) {
  const user = this;
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
      user.password = hash;
      next();
    });
  });
});

module.exports = mongoose.model('User', userSchema);
