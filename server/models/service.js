const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const serviceSchema = new Schema({
  company: {
    type: String,
    required: true,
    max: [128, "Too long, max is 128 characters."]
  },
  category: { type: String, required: true },
  city: {
    type: String,
    required: true,
    lowercase: true
  },
  state: {
    type: String,
    required: true
  },
  image: { type: String, required: true },
  jobsCompleted: Number,
  rating: Number,
  recommended: Boolean,
  description: { type: String, required: true },
  price: Number,
  createdAt: { type: Date, default: Date.now },
  user: { type: Schema.Types.ObjectId, ref: "User" },
  bookings: [{ type: Schema.Types.ObjectId, ref: "Booking" }]
});

module.exports = mongoose.model("Service", serviceSchema);
