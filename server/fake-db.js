const Service = require("./models/service");
const User = require("./models/user");
const Booking = require("./models/booking");

const fakeDbData = require("./data.json");

class FakeDb {
  constructor() {
    this.services = fakeDbData.services;

    this.users = fakeDbData.users;
  }

  async cleanDb() {
    await Service.deleteMany({});
    await User.deleteMany({});
    await Booking.deleteMany({});
  }

  pushDataToDb() {
    const user = new User(this.users[0]);
    const user1 = new User(this.users[1]);

    this.services.forEach(service => {
      const newService = new Service(service);
      newService.user = user;

      user.services.push(newService);

      newService.save();
    });

    user.save();
    user1.save();
  }

  async seedDb() {
    await this.cleanDb();
    this.pushDataToDb();
  }
}

module.exports = FakeDb;
