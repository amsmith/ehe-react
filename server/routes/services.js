const express = require('express');
const router = express.Router();
const Service = require('../models/service');
const User = require('../models/user');
const { normalizeErrors } = require('../helpers/mongoose');
const UserCtrl = require('../controllers/user');

router.get('/secret', UserCtrl.authMiddleware, (req, res) => {
  res.json({ user: true });
});

// UPDATE SERVICE
router.get('/manage', UserCtrl.authMiddleware, function(req, res) {
  const user = res.locals.user;

  Service.where({ user })
    .populate('bookings')
    .exec(function(err, foundServices) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }
      return res.json(foundServices);
    });
});

// SERVICE DETAIL
router.get('/:id/verify-user', UserCtrl.authMiddleware, function(req, res) {
  const user = res.locals.user;

  Service.findById(req.params.id)
    .populate('user')
    .exec(function(err, foundService) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      if (foundService.user.id !== user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid User',
              detail: 'You are not the owner of this Service.'
            }
          ]
        });
      }

      return res.json({ status: 'verified' });
    });
});

router.get('/:id', function(req, res) {
  const serviceId = req.params.id;

  Service.findById(serviceId)
    .populate('user', 'username -_id')
    .populate('bookings', 'startAt endAt -_id')
    .exec(function(err, foundService) {
      if (err) {
        return res.status(422).send({
          errors: [
            { title: 'Service Error', detail: 'Could not find Service.' }
          ]
        });
      }
      return res.json(foundService);
    });
});

// UPDATE SERVICE ROUTE
router.patch('/:id', UserCtrl.authMiddleware, function(req, res) {
  const serviceData = req.body;
  const user = res.locals.user;

  Service.findById(req.params.id)
    .populate('user')
    .exec(function(err, foundService) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      if (foundService.user.id !== user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid User',
              detail: 'You are not the owner of this Service.'
            }
          ]
        });
      }

      foundService.set(serviceData);
      foundService.save(function(err) {
        if (err) {
          return res.status(422).send({ errors: normalizeErrors(err.errors) });
        }
        return res.status(200).send(foundService);
      });
    });
});

// DELETE SERVICE ROUTE
router.delete('/:id', UserCtrl.authMiddleware, function(req, res) {
  const user = res.locals.user;

  Service.findById(req.params.id)
    .populate('user', '_id')
    .populate({
      path: 'bookings',
      select: 'startAt',
      match: { startAt: { $gt: new Date() } }
    })
    .exec(function(err, foundService) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }
      if (user.id !== foundService.user.id) {
        return res.status(422).send({
          errors: [
            {
              title: 'Invalid User',
              detail: 'You are not the owner of this Service.'
            }
          ]
        });
      }

      if (foundService.bookings.length > 0) {
        return res.status(422).send({
          errors: [
            {
              title: 'Active Bookings',
              detail: 'Cannot Delete a Service with Active Bookings.'
            }
          ]
        });
      }

      foundService.remove(function(err) {
        if (err) {
          return res.status(422).send({ errors: normalizeErrors(err.errors) });
        }
        return res.json({ status: 'deleted' });
      });
    });
});

// CREATE SERVICE
router.post('', UserCtrl.authMiddleware, (req, res) => {
  const {
    company,
    category,
    city,
    state,
    image,
    jobsCompleted,
    rating,
    recommended,
    description,
    price
  } = req.body;
  const user = res.locals.user;
  const service = new Service({
    company,
    category,
    city,
    state,
    image,
    jobsCompleted,
    rating,
    recommended,
    description,
    price
  });
  service.user = user;

  Service.create(service, function(err, newService) {
    if (err) {
      return res.status(422).send({ errors: normalizeErrors(err.errors) });
    }

    User.updateOne(
      { _id: user.id },
      { $push: { services: newService } },
      function() {}
    );
    return res.json(newService);
  });
});

// SERVICES LIST
router.get('', function(req, res) {
  const city = req.query.city;

  if (city) {
    Service.find({ city: city.toLowerCase() })
      .select('-bookings')
      .exec(function(err, filteredServices) {
        if (err) {
          return res.status(422).send({ errors: normalizeErrors(err.errors) });
        }
        if (filteredServices.length === 0) {
          return res.status(422).send({
            errors: [
              {
                title: 'No Services Found',
                detail: `There are no services for ${city}`
              }
            ]
          });
        }
        return res.json(filteredServices);
      });
  } else {
    Service.find({})
      .select('-bookings')
      .exec(function(err, foundServices) {
        return res.json(foundServices);
      });
  }
});

module.exports = router;
